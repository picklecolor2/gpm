## 0.1.4

Fixed bad links in gpackage.json

## 0.1.3

Fixed bug when converting a timestamp to an ISO8601 string in non-negative time zones.

## 0.1.2

Removed UTF converter when on RT.

## 0.1.1

Fixed NaN/Inf case for numbers.

## 0.1.0

Initial Release.
