# Progress Bar API

#### _This package is implemented with LabVIEW 2017_

The Progress Bar API provides a set of common features that can be used to implement a nice, consistent looking progress bar to the user.

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.
